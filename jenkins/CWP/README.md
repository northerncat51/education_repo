### build
```
java -jar custom-war-packager-cli-1.5-jar-with-dependencies.jar -configPath=packager-config.yml -tmpDir=tmp/
```

### run
```
docker run -p 8889:8080 jenkins/custom-war-packager
```
