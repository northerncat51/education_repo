#! /bin/bash

clear

# creating users from txt file

echo -e "===\nCreating users from users.txt file\n==="
if [ -f users.txt ]
then
        while read LINE;
                do useradd -s /bin/bash $LINE
                done < users.txt
else echo -e "NO file users.txt\n"
fi
echo -e "All users are added\n"

# Creating groups for users above

echo -e "\n===\nCreating groups for users above\n==="
for (( i=1; i<=3; i++ ))
do
        groupadd proj$i && echo -e "Group proj$i has been added\n"
done
echo -e "Groups adding completed\n"

# Adding users to required groups

echo -e "===\nAdding users to appropriate groups\n==="

#proj1
for user in R2 R3 R5 A1
do
	usermod -a -G proj1 "$user" && echo -e "User $user has been added to proj1"
done

#proj2
for user in R1 R5 A1
do
        usermod -a -G proj2 "$user" && echo -e "User $user has been added to proj2"
done

#proj3
for user in R1 R2 R4 A2
do
        usermod -a -G proj3 "$user" && echo -e "User $user has been added to proj3"
done

# Creating folders

echo -e "===\nCreating folders for projects\n==="

for (( i=1; i<=3; i++ ))
	do
		if [ ! -d "project$i" ]
		then
			mkdir project$i
			echo "Directory project$i created"
		else
			echo "Directory project$i already created"
		fi
	done
echo -e "===\n"

# Assinging rights to folders

echo -e "Assigning rights on projects\n"
for (( i=1; i<=3; i++ ))
do
	chown :proj$i project$i
	chmod 2770 project$i
done
echo -e "Done\n===\n"

# Assigning right for rest of users

echo -e "Assigning default ACL\n"
setfacl -d -m u::rwx,g::rwx,o::-,u:A4:rx project1
setfacl -m u:A4:rx project1
setfacl -d -m u::rwx,g::rwx,o::-,u:A2:rx,u:A3:rx project2
setfacl -m u:A2:rx,u:A3:rx project2
setfacl -d -m u::rwx,g::rwx,o::-,u:A1:rx,u:A4:rx project3 
setfacl -m u:A1:rx,u:A4:rx project3
echo -e "Done\n==="

echo -e "\n===\nScript has ended\nPlease verify result\n==="
