class Employee:

    def __init__(self, first_name, last_name, salary):
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary

    @property
    def full_name(self):
        return f'{self.first_name}, {self.last_name}'

    @property
    def email(self):
        return f'{self.first_name}_{self.last_name}@example.com'

