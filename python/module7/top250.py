import bs4
import requests
import json


def parse_top_250(func):
    headers = {"Accept-Language": 'En-us'}
    res = requests.get("https://imdb.com/chart/top", headers=headers)
    soup = bs4.BeautifulSoup(res.text, "lxml")
    res_list = []
    rating_list = []
    for item2 in soup.find_all('td', class_='ratingColumn imdbRating'):
        rating_list.append(item2.contents[1].text)

    for item in soup.find_all('td', class_='titleColumn'):
        position = int((item.contents[0]).strip().replace('.', ''))
        str_crew = (item.find('a').get('title'))
        dir_, crew = str_crew.split(' (dir.), ')
        res_list.append({
            item.a.text: {
                'Position': str(position),
                'Year': item.span.text[1:5],
                'Director': dir_,
                'Crew': crew,
                'Rating': str(rating_list[position - 1])
            }}
        )

    with open("top250.json", 'w', encoding="utf8") as new_file:
        json.dump(res_list, new_file)


# parse_top_250('top250.json')
