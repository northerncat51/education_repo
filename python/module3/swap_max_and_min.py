def swap_max_and_min(res):
    try:
        max_index = res.index(max(res))
        min_index = res.index(min(res))
        temp_value = res[max_index]
        res[max_index] = res[min_index]
        res[min_index] = temp_value
        if len(res) != len(set(res)):
            raise ValueError
        return (res)
    except TypeError:
        raise TypeError