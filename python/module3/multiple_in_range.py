def multiple_in_range(a,b):
    try:
        res=[]
        for i in range(a,b+1):
            if i % 7 == 0 and i % 5 != 0:
                res.append(i)
        return(res)
    except ValueError:
        raise Exception("Only integer numbers")