def lists_2_dict(a,b):
    try:
        res = dict(zip(a, b))
        return res
    except TypeError:
        raise TypeError

lists_2_dict(["Name", "Age", "Job Title"], ["Jane", 44, "Python Developer"])