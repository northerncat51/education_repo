def dict_swap(res):
    try:
        s = {val: key for (key, val) in res.items()}
        return s
    except ValueError:
        raise ValueError