from functools import wraps
from time import sleep

def delay(func):
    @wraps(func)
    def wrapper():
        sleep(4)
        func()
    return wrapper
