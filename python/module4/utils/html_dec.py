from functools import wraps


def italic(func):
    @wraps(func)
    def wrapper(*args):
        return f'<i>{func(*args)}</i>'
    return wrapper


def bold(func):
    @wraps(func)
    def wrapper(*args):
        return f'<b>{func(*args)}</b>'
    return wrapper


def underline(func):
    @wraps(func)
    def wrapper(*args):
        return f'<u>{func(*args)}</u>'
    return wrapper


@bold
@italic
@italic
def print_text(s):
    return s


# print(print_text(input("Enter whatever to convert: ")))