import operator
import re
from collections import Counter


class LogParser:

    def __init__(self, file_name):
        self.file_name = file_name
        pass

    def log_by_http_code(self, file_name, code):
        pattern = re.compile(f"\s+{code}\s+")
        with open('access.log', 'r') as read_file:
            with open(file_name, 'w') as write_file:
                for line in read_file.readlines():
                    if pattern.findall(line):
                        write_file.write(line)

    def get_most_common(self, deep):
        pattern = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
        with open('access.log', 'r') as read_file:
            log = read_file.read()
            ip_list = re.findall(pattern, log)
            ipcount = Counter(ip_list)
            cd = sorted(ipcount.items(), key=operator.itemgetter(1), reverse=True)
        return cd[0:deep]