import csv


class CsvParser:

    def __init__(self, file_name):
        pass


    def save_as(self, file_name, delim):
        self.file_name = file_name
        self.delim = delim
        with open('1000 Sales Records.csv', 'r') as origin_file:
            reader = csv.reader(origin_file)
            with open(self.file_name, 'w') as copy_file:
                writer = csv.writer(copy_file, delimiter=self.delim)
                writer.writerows(reader)

    def get_country_profit(self, country):
        with open('1000 Sales Records.csv', 'r') as origin_file:
            reader = csv.reader(origin_file)
            summary = 0
            for line in reader:
                if line[1] == country:
                    summary = summary + float(line[13])
        return summary

    def sell_over(self, category, amount):
        with open('1000 Sales Records.csv', 'r') as origin_file:
            reader = csv.reader(origin_file)
            country_list = []
            for line in reader:
                if line[2] == category and int(line[8]) >= amount:
                    country_list.append(line[1])
        country_list.sort()
        return country_list
